#include <QtGui/QApplication>
#include <QtDeclarative/QDeclarativeView>
#include <QtDeclarative/QDeclarativeContext>
#include <QtDeclarative/QDeclarativeEngine>
#include <QDebug>

#include "myclass.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QDeclarativeView canvas;

    MyClass *objectMyClass = new MyClass();

    canvas.engine()->rootContext()->setContextObject(objectMyClass);
    canvas.setSource(QString("qrc:qml/QMLCPPJavaScript/main.qml"));

    QObject::connect(canvas.engine(), SIGNAL(quit()), &app, SLOT(quit()));

    canvas.setResizeMode(QDeclarativeView::SizeRootObjectToView);
    canvas.showFullScreen();

    return app.exec();
}
