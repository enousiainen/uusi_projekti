import QtQuick 1.0
import "JavaScript/MyJSFunctions.js" as MyJavaScript

Rectangle {
    width: 360; height: 360; color:"blue"

    property string myString : "Hello Second Text"

    Image {
        source:"Pics/qtlogo.png"
        y:760; x:400
    }

    Text {
        id: myText
        text: qsTr("Hello World")
        anchors.centerIn: parent
        font.pixelSize: 35
    }

    Text {
        id: mySecondText
        text: myString
        anchors.horizontalCenter: parent.horizontalCenter
        font.pixelSize: 35
    }
    MouseArea {
        anchors.fill: parent
        onClicked: {
            MyJavaScript.myJavaScriptFunction("Hey");
            myString=myCPPFunction("Hey"); // toimiii myos seuraava:  mySecondText.text = myCPPFunction("Hey");
        }
    }
}
